#!/bin/bash

##########################################################
# 1. unzip app
# 2. check composer -> download (if not exists) + update
# 3. check python3 + pip3
# 4. install eth-keyfile
##########################################################

# app package name
app_package='eth-bot.zip'

cd $PWD

############## Unzip application ##############

# check is .zip file exist
if [[ ! -f $PWD/$app_package ]]; then
    echo "Package file not found!"
    echo "Copy 'eth-app.zip' in current directory and re-run this script"
    exit 0
fi

# check is unzip exists
if ! [[ -x "$(command -v unzip)" ]]; then
    echo ""
    echo " ##### Installing unzip ##### "
    echo ""

    apt install unzip
fi

# unzip package
echo ""
echo " ##### Unzipping package ##### "
echo ""

unzip $app_package -d $PWD

cd $PWD/core

# change rights for sys/cache folders
chmod 777 storage -R
chmod 777 bootstrap -R


############## Composer ###################

# check is composer exists
if ! [[ -x "$(command -v composer)" ]]; then
    # install composer
    echo ""
    echo " ##### Installing composer ##### "
    echo ""

    php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
    php composer-setup.php --filename=composer --install-dir=$PWD
    php -r "unlink('composer-setup.php');"
fi

# try to install gmp for sure
apt update
apt install php-gmp

echo ""
echo " ##### Dump autoload ##### "
echo ""

./composer dump-autoload


############## Python3 + pip3 ##############

# check python3
if ! [[ -x "$(command -v python3)" ]]; then
    # install python3
    echo ""
    echo " ##### Installing python3 ##### "
    echo ""

    apt install python3
fi

# check pip3
if ! [[ -x "$(command -v pip3)" ]]; then
    # install pip3
    echo ""
    echo " ##### Installing pip3 ##### "
    echo ""

    apt install python3-pip
fi

echo ""
echo " ##### Updating vendors ##### "
echo ""

pip3 install eth-keyfile

echo "\n"
echo "Deploy completed!"