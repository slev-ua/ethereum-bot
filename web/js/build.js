$(document).ready(function() {

    /****************** Tab switcher *******************/
    $(".nav-link").click(function(e) {
        e.preventDefault();

        $(".nav-link").removeClass('active');
        $(".tabs").hide();

        let target = $(this).data('target');
        $(this).addClass('active');

        $("."+target).show();
    });

    /****************************************************/

    /******************** Application *******************/

    app.init();

    $(document).on('click', '[app-target]', function(e) {
        let method = $(this).attr('app-target');
        app[method]();

        $(this).attr('disabled', true);
        func = function(obj) {
            $(obj).attr('disabled', false);
        };
        setTimeout(func, 3000, this);
    });

    $(document).on('change', '[app-mutable]', function(e) {
        let method = $(this).attr('app-mutable');
        // event target as param
        app[method](this);
    });

    /*****************************************************/

});
