
// Eth-Bot js application

let app = {

    endpoints: {
        decryptWallet:  './decryptWallet',
        contractsList:  './contractsList',
        parseToken:     './parseToken',
        grabTokens:     './grabTokens',
        sendEth:        './sendEth',
        sendAllEth:     './grabAllEth',
    },

    /** Laravel csrf token */
    csrf_token: null,

    /** Wallet's address/private key, erc20 balance, */
    decryptedWallet: null,
    erc20Tokens: {},
    contractsList: null,

    processedContractsCount: 0,

    /**
     * Standard transaction fee, in ETH
     * 21000 gas * 4Gwei
     */
    standardTxFee: 0.000084,

    txTotalPrice: 0,

    /**
     * Application init
     */
    init: function() {
        this.parseCsrfToken();
    },

    /**
     * Get decryption data and try to decrypt wallet.
     *
     * Decrypt means checking is provided private key or
     * keystore + pwd pair was correct, parse wallet address (and private key)
     * from provided data.
     */
    decryptWallet: function() {
        app.wipeStatisticData();

        let decryption_data = this.getDataForDecryption();
        if(decryption_data.status === "error") {
            alert(decryption_data.description);
            return false;
        }

        // show decrypt tab spinner
        $(".unlock.loading").show();

        let endpoint = this.endpoints.decryptWallet;

        this.request('POST', endpoint, decryption_data, function(result) {
            if(result.status === "error") {
                console.log("Can't decrypt wallet. Description: "+result.description);
                alert(result.description);
                // hide decrypt tab spinner
                $(".unlock.loading").hide();
                return false;
            } else if(result.status === 'network_error') {
                alert("Ошибка сети, попробуйте ещё раз.");
                // hide decrypt tab spinner
                $(".unlock.loading").hide();
                return false;
            }

            // hide decrypt tab spinner
            $(".unlock.loading").hide();

            delete result.status;
            app.decryptedWallet = result;
            app.redrawDecryptedData();
        });
    },

    /**
     * Fill required inputs with decrypted wallet address
     */
    redrawDecryptedData: function() {

        // inputs
        $(".erc20-source").val(this.decryptedWallet.address);
        $("[name='send_eth_from']").val(this.decryptedWallet.address);

        let max_send_amount = this.decryptedWallet.balance - this.standardTxFee;
        if(max_send_amount > 0) {
            $("[name='send_eth_amount']").attr('disabled', false)
                .attr('placeholder', 'Amount (' + max_send_amount + ' max)')
                .val("");
        } else {
            $("[name='send_eth_amount']").attr('disabled', true).val(0);
        }


        // decryption tab table
        $(".t-w-addr").html(this.decryptedWallet.address);
        $(".t-w-priv").html(this.decryptedWallet.private);
        $(".t-w-bal").html(this.decryptedWallet.balance + " ETH");
    },

    /**
     * Detect decryption method, returns required data
     * @return {*}
     */
    getDataForDecryption: function() {

        let priv = $(".decrypt-private").val();
        if(priv.length === 64) {
            return {
                status: 'ok',
                method: 'priv',
                priv: priv
            };
        }


        let keystore = $(".decrypt-keystore").val();
        let keystore_pwd = $('.decrypt-keystore-pwd').val();

        if(keystore.length > 100 && keystore_pwd !== "")
            return {
                status: 'ok',
                method: 'keystore',
                keystore: keystore,
                password: keystore_pwd
            };

        return {
            status: 'error',
            description: 'Choose decryption method'
        }
    },

    /**
     * Get contracts list from wallet coins operation
     * @return {[void]}
     */
    getContractsList: function() {

        app.txTotalPrice = 0;
        app.wipeStatisticData();
        app.updateStatusTerminal('request-contracts');

        let endpoint = this.endpoints.contractsList;
        let args = {wallet: this.decryptedWallet.address, _token: this.csrf_token};
        this.request("POST", endpoint, args, function(result) {

            // if failed or empty
            if(result.status === 'failed') {

                if(result.description === "No transactions found") {
                    alert('Wallet has not ERC20 tokens');
                    app.updateStatusTerminal('contracts-received', 0);
                    return false;
                }

                alert("Can't find wallet by provided address");
                // hide erc20 tab spinner
                $(".erc20.loading").hide();
                return false;
            } else if(result.status === "empty") {
                alert('Wallet has not ERC20 tokens');
                app.updateStatusTerminal('contracts-received', 0);
                return false;
            }

            app.contractsList = result.result;
            app.updateStatusTerminal('contracts-received', result.result.length);
            
            $.each(app.contractsList, function(item, data) {
                setTimeout(function() {
                    app.getErc20TokenData(data);
                }, 300 * item);
            });
        });
    },

    /**
     * Get token data for current wallet (by contract)
     * @param  {[string]} contract_address
     * @return {[void]}                  
     */
    getErc20TokenData: function(contract_address) {
        let endpoint = this.endpoints.parseToken;
        let args = {
            wallet: this.decryptedWallet.address, 
            contract_address: contract_address,
            _token: this.csrf_token
        };

        this.request("POST", endpoint, args, function(result) {
            app.processedContractsCount++;
            app.updateStatusTerminal('processing-data', {
                done: app.processedContractsCount,
                total: app.contractsList.length
            });

            // if failed or empty
            if(result.status === 'failed') {
                if(result.description === "No transactions found") {
                    return false;
                }

                alert("Can't find wallet by provided address");
                return false;
            } else if(result.status === "empty") {
                return false;
            }

            app.erc20Tokens[app.processedContractsCount] = result.result[0];
            app.drawStatTable(result.result[0], app.processedContractsCount);
        });
    },

    /**
     * Mark concrete token (by id in list)
     * for future send transaction
     * @param obj
     */
    markErc20ForSend: function(obj) {

        let checked = $(obj).prop('checked');
        let id = $(obj).data('id');

        if(this.erc20Tokens === null)
            return;

        if(this.erc20Tokens[id] === 'undefined')
            return;

        this.erc20Tokens[id].send = checked;
    },

    /**
     * Mark all erc20 tokens for send
     * @param obj
     */
    markAllErc20: function(obj) {

        let check = $(obj).prop('checked');

        $('input[data-id]').each(function(key, item) {

            $(item).prop('checked', check);
            app.markErc20ForSend(item);

        });
    },

    /**
     * Send marked erc20 tokens
     */
    sendErc20: function() {

        app.txTotalPrice = 0;

        // recipient address
        let to_obj = $("[name='send_erc20_to']");
        let to = $(to_obj).val();
        if(to.length !== 42) {
            $(to_obj).addClass("text-danger");
            return false;
        }
        $(to_obj).removeClass("text-danger");

        // show erc20 tab spinner
        $(".erc20.loading").show();

        args = {
            tokens: app.erc20Tokens,
            from: app.decryptedWallet.address,
            priv: app.decryptedWallet.private,
            to: to,
        };

        let endpoint = app.endpoints.grabTokens;
        app.request("POST", endpoint, args, function(result) {

            if(result.status !== "success") {
                alert("Tx Failed!\nReason: "+result.description);
                $(".erc20.loading").hide();
                return false;
            }

            app.erc20Tokens = result.tokens;

            app.wipeStatisticData();
            $.each(app.erc20Tokens, function(item, data) {
                app.drawStatTable(data, item);
            });

            $(".erc20.loading").hide();
        });
    },

    /**
     * Send eth from unlocked account
     * @return {boolean}
     */
    sendEth: function() {

        if(!this.isDecrypted()) {
            alert("Unlock wallet first!");
            return false;
        }

        let from = this.decryptedWallet.address;
        let priv = this.decryptedWallet.private;

        // check 'to' address
        let to_obj = $("[name='send_eth_to']");
        let to = $(to_obj).val();
        if(to.length !== 42) {
            $(to_obj).addClass('text-danger');
            return false;
        }
        $(to_obj).removeClass('text-danger');

        // check 'amount' val
        let amount_obj = $("[name='send_eth_amount']");
        amount = parseFloat($(amount_obj).val());
        if(amount <= 0) {
            alert("Amount must be greater than 0");
            return false;
        }
        if((amount + this.standardTxFee) > this.decryptedWallet.balance) {
            alert("Amount + Tx Fee greater than wallet balance");
            return false;
        }

        // show eth tab spinner
        $(".eth.loading").show();

        let args = {
            from:   this.decryptedWallet.address,
            priv:   this.decryptedWallet.private,
            to:     to,
            amount: amount
        };
        let endpoint = this.endpoints.sendEth;

        this.request("POST", endpoint, args, function(result) {
            if(result.status !== "success") {
                alert("Transaction failed!\nReason - "+result.description);
                // hide eth tab spinner
                $(".eth.loading").hide();
                return false;
            }

            // hide eth tab spinner
            $(".eth.loading").hide();

            alert(
                "Transaction send!" +
                "\nWait at least 1 confirmation, then update wallet balance!" +
                "\nTxHash - "+result.txHash
            );
        });
    },

    /**
     * Send all available eth
     * @return {void}
     */
    grabAllEth: function() {

        if(!this.isDecrypted()) {
            alert("Unlock wallet first!");
            return false;
        }

        let from = this.decryptedWallet.address;
        let priv = this.decryptedWallet.private;

        // check 'to' address
        let to_obj = $("[name='send_eth_to']");
        let to = $(to_obj).val();
        if(to.length !== 42) {
            $(to_obj).addClass('text-danger');
            return false;
        }
        $(to_obj).removeClass('text-danger');

        // show eth tab spinner
        $(".eth.loading").show();

        let args = {
            from:   this.decryptedWallet.address,
            priv:   this.decryptedWallet.private,
            to:     to,
        };
        let endpoint = this.endpoints.sendAllEth;

        this.request("POST", endpoint, args, function(result) {
            if(result.status !== "success") {
                alert("Transaction failed!\nReason - "+result.description);
                // hide eth tab spinner
                $(".eth.loading").hide();
                return false;
            }

            // hide eth tab spinner
            $(".eth.loading").hide();

            alert(
                "Transaction send!" +
                "\nWait at least 1 confirmation, then update wallet balance!" +
                "\nTxHash - "+result.txHash
            );
        });
    },

    /**
     * Clear status terminal, clear stat, wipe processed contracts count
     * @return {void}
     */
    wipeStatisticData: function() {
        app.txTotalPrice = 0;
        app.wipeStatusTerminal();
        app.wipeStatTable();
        app.processedContractsCount = 0;
    },

    /**
     * Update status terminal - show/update target message
     * @param  {string} target  class of target message
     * @param  {mixed}  payload msg payload (null by default)
     * @return {void}         
     */
    updateStatusTerminal: function(target, payload = null) {
        let html;

        switch(target) {

            case "request-contracts":
                html = "Requesting contracts list...";
                break;

            case "contracts-received":
                html = "Found <strong>"+payload+"</strong> unique contract addresses!";
                break;

            case "processing-data":
                html = "Processed <strong>"+payload['done']+"/"+payload['total']+"</strong>";
                break;

            default: return;    
        }

        $(".processing."+target).html(html).show();
    },

    /**
     * Clear terminal messages
     * @return {[type]} [description]
     */
    wipeStatusTerminal: function() {
        $(".request-contracts").hide().html('');
        $(".contracts-received").hide().html('');
        $(".processing-data").hide().html('');
    },

    /**
     * Draw/redraw tokens stat table
     * Add new row, when new token data received
     * @param  {object} token token data object
     * @param k
     * @return {void}
     */
    drawStatTable: function(token, k) {

        $(".empty-state").html('');
        let html = "<tr>";

        html += "<td align='center'>"+token.tokenName+"</td>\n";
        html += "<td align='center'>"+token.tokenSymbol+"</td>\n";

        html += "<td align='center' " +
            "data-name='tok-balance'>" +
            parseFloat(token.convertedTokenBalance).toFixed(5) + " ("+token.tokenSymbol+")</td>\n";

        html += (token.requiredGasPrice < 0)?
            "<td align='center'>Can't calculate gas</td>" :
            "<td align='center'>"+parseFloat(token.requiredGasPrice).toFixed(5)+" eth</td>";

        html += "<td align='center'>";
        if(typeof token.status === 'undefined' || token.status !== 'success') {
            if(token.requiredGasPrice > -1)
                html += "<input type='checkbox' app-mutable='markErc20ForSend' data-id='" + k + "'";
        }

        html += '</td>';

        if(typeof token.status !== 'undefined') {
            html += (token.status === 'success')?
                "<td align='center'><span title='Successfully sent'>"+app.icons('success')+"</span></td>" :
                "<td align='center'><span title='Error! "+token.description+"'>"+app.icons('error')+"</span></td>";
        } else {
            if(token.requiredGasPrice > 0)
                app.txTotalPrice += parseFloat(token.requiredGasPrice);
        }

        $(".tx-total-cost").text(parseFloat(app.txTotalPrice).toFixed(5)+" eth");

        $(".wallet-stat").append(html);
    },

    /**
     * Wipe stat table and draw empty-state row
     * @return {[type]} [description]
     */
    wipeStatTable: function() {
        let html = "<tr class='empty-state'>";
        html += "<td colspan='6' style='text-align: center'>Empty</td>";
        html += "</tr>";

        $(".wallet-stat").html(html);

        $(".tx-total-cost").text("Total Tx Cost");
    },

    /**
     * Make request to endpoint
     * @param {string} method
     * @param {string} endpoint
     * @param {object} args
     * @param {function} callable
     */
    request: function(method, endpoint, args, callable) {

        if(this.csrf_token === null) {
            alert("Can't parse laravel's token, refresh page!");
            return false;
        }

        args._token = app.csrf_token;

        $.ajax({
            method: method,
            url: endpoint,
            data: args
        })
            .done(function(response) {
                callable(response);
            });
    },

    /**
     * Parse Laravel's csrf token
     * @return {boolean|string}
     */
    parseCsrfToken: function() {

        let _token = $("[name='_token']").val();
        if(!_token) {
            console.log("Fatal: Can't parse CSRF token!");
            return false;
        }

        this.csrf_token = _token;
    },

    /**
     * Is wallet decrypted?
     * @return {boolean}
     */
    isDecrypted: function() {
        return !(null === this.decryptedWallet);
    },

    /**
     * Escape '0x' hex code from string
     * @param {string} string
     * @returns {string}
     */
    escapeHex: function(string) {
        return string.replace('0x', '_');
    },

    /**
     * Return error|success svg icon codes
     * @param type
     * @return {string}
     */
    icons: function(type) {

        // error svg icon code
        if(type === "error")
            return '<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px"\n' +
                'width="26" height="26"\n' +
                'viewBox="0 0 224 224"\n' +
                'style=" fill:#000000;">' +
                '<g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" stroke-linecap="butt" ' +
                'stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" stroke-dashoffset="0" ' +
                'font-family="none" font-weight="none" font-size="none" text-anchor="none" style="mix-blend-mode: normal">' +
                '<path d="M0,224v-224h224v224z" fill="none"></path>' +
                '<g fill="#e74c3c"><g id="surface1">' +
                '<path d="M111.3,15.4c-1.365,0.21 -2.555,1.0325 -3.22,2.24l-107.52,186.2c-0.805,1.4 -0.805,3.115 ' +
                '0.0175,4.4975c0.805,1.3825 2.2925,2.24 3.9025,2.2225h215.04c1.61,0.0175 3.0975,-0.84 ' +
                '3.9025,-2.2225c0.8225,-1.3825 0.8225,-3.0975 0.0175,-4.4975l-107.52,-186.2c-0.91,-1.645 ' +
                '-2.765,-2.5375 -4.62,-2.24zM112,28.84l99.68,172.76h-199.36zM106.82,84.7c-0.9625,0 -1.54,0.5775 ' +
                '-1.54,1.54v64.82c0,0.9625 0.5775,1.68 1.54,1.68h10.36c0.9625,0 1.54,-0.7175 1.54,-1.68v-64.82c0,-0.9625 ' +
                '-0.5775,-1.54 -1.54,-1.54zM105.84,165.62c-0.595,0.2275 -0.84,0.8225 -0.84,1.54v11.76c0,0.9625 0.5775,1.54 ' +
                '1.54,1.54h10.92c0.9625,0 1.54,-0.5775 1.54,-1.54v-11.76c0,-0.9625 -0.5775,-1.54 ' +
                '-1.54,-1.54h-10.92c-0.245,0 -0.5075,-0.07 -0.7,0z"></path></g></g></g></svg>';


        // success svg icon code
        return '<svg xmlns="http://www.w3.org/2000/svg" x="0px" y="0px" width="26" height="26" viewBox="0 0 224 224" ' +
            'style=" fill:#000000;"><g fill="none" fill-rule="nonzero" stroke="none" stroke-width="1" ' +
            'stroke-linecap="butt" stroke-linejoin="miter" stroke-miterlimit="10" stroke-dasharray="" ' +
            'stroke-dashoffset="0" font-family="none" font-weight="none" font-size="none" text-anchor="none" ' +
            'style="mix-blend-mode: normal"><path d="M0,224v-224h224v224z" fill="none"></path><g fill="#2ecc71">' +
            '<path d="M112,13.44c-54.38012,0 -98.56,44.17988 -98.56,98.56c0,54.38012 44.17988,98.56 98.56,98.56c54.38012,0 ' +
            '98.56,-44.17988 98.56,-98.56c0,-54.38012 -44.17988,-98.56 -98.56,-98.56zM112,22.4c49.53778,0 89.6,40.06222 ' +
            '89.6,89.6c0,49.53778 -40.06222,89.6 -89.6,89.6c-49.53778,0 -89.6,-40.06222 -89.6,-89.6c0,-49.53778 40.06222,-89.6 ' +
            '89.6,-89.6zM156.7475,67.1475c-1.47795,0.02913 -2.84642,0.78539 ' +
            '-3.6575,2.02125l-45.70125,67.36625l-32.66375,-30.31c-1.16787,-1.12377 -2.85159,-1.52923 ' +
            '-4.403,-1.06028c-1.55141,0.46895 -2.72862,1.73918 -3.07845,3.32171c-0.34983,1.58253 0.18226,3.23062 ' +
            '1.39144,4.30981l40.3025,37.3975l51.5725,-76.0025c0.96456,-1.3821 1.07191,-3.18857 0.27782,-4.67517c-0.79409,-1.4866 ' +
            '-2.35525,-2.40182 -4.04032,-2.36858z"></path></g></g></svg>';

    },
};