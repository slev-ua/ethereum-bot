@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-10">
            <div class="card">
                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                    {{ csrf_field() }}

                    <div class="col-md-12 mb-5">

                        <ul class="nav nav-tabs">
                            <li class="nav-item">
                                <a class="nav-link active" data-target="tab-unlock">Unlock Acc</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-target="tab-eth">Send Eth</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" data-target="tab-erc20">Get ERC20</a>
                            </li>
                        </ul>

                    </div>

                    <div class="container">
                        <div class="col-md-12 tabs tab-unlock">
                            <div class="unlock loading"></div>
                            <div class="col-md-8">
                                <div class="row mb-3">
                                    <input class="form-control decrypt-private" type="text" style="width:600px"
                                           placeholder="Insert private key">
                                </div>

                                <div class="row mb-3"><b>OR</b></div>

                                <div class="row mb-3">
                                    <textarea class="form-control decrypt-keystore mb-2" placeholder="Keystore data here"></textarea>
                                    <input class="form-control decrypt-keystore-pwd" type="text" style="width:600px"
                                           placeholder="Insert keystore password">
                                </div>

                                <div class="row mb-3">
                                    <button class="btn btn-success" app-target="decryptWallet">Decrypt</button>
                                </div>
                            </div>


                                    <table class="table table-responsive">
                                        <tr>
                                            <td>Address:</td>
                                            <td class="t-w-addr" style="font-weight: bold"></td>
                                        </tr>
                                        <tr>
                                            <td>Private key:</td>
                                            <td class="t-w-priv" style="font-weight: bold"></td>
                                        </tr>
                                        <tr>
                                            <td>Balance:</td>
                                            <td class="t-w-bal" style="font-weight: bold"></td>
                                        </tr>
                                    </table>


                        </div>

                        <div class="col-md-12 tabs tab-eth" style="display: none">
                            <div class="eth loading"></div>
                            <div class="col-md-8">
                                <div class="row mb-3">
                                    <input class="form-control" type="text" name="send_eth_from" disabled style="width:600px"
                                           placeholder="From">
                                </div>

                                <div class="row mb-3">
                                    <input class="form-control" type="text" name="send_eth_to" style="width:600px"
                                           placeholder="To">
                                </div>

                                <div class="row mb-3">
                                    <button class="btn btn-success" app-target="grabAllEth">Send All</button>
                                </div>

                                <div class="row mb-3"><b>OR</b></div>

                                <div class="row mb-3">
                                    <input class="form-control" type="text" name="send_eth_amount" style="width:600px"
                                           placeholder="Amount">
                                </div>

                                <div class="row mb-3">
                                    <button class="btn btn-success" app-target="sendEth">Send</button>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-12 tabs tab-erc20" style="display: none;">
                            <div class="erc20 loading"></div>
                            <div class="row">
                                <div class="col-md-8">
                                    <div class="row mb-3">
                                        <input class="form-control erc20-source" type="text" name="erc20-source" disabled style="width:600px"
                                               placeholder="Target wallet">
                                    </div>

                                    <div class="row mb-3">
                                        <button class="btn btn-success" app-target="getContractsList">Inspect</button>
                                    </div>
                                </div>

                                <div class="col-md-4 highlight term-emulated">
                                    <div class="processing request-contracts hidden"></div>
                                    <div class="processing contracts-received hidden"></div>
                                    <div class="processing processing-data hidden"></div>
                                </div>
                            </div>

                            <div class="col-md-12 mb-5 stat-block">
                                <div class="row">
                                    <table class="table table-responsive stat-table">
                                        <thead>
                                            <tr>
                                                <th>Token Name</th>
                                                <th>Token Symbol</th>
                                                <th>Balance</th>
                                                <th class="tx-total-cost">Total Tx Cost</th>
                                                <th class="send-erc20-all"><input type='checkbox' app-mutable='markAllErc20'/></th>
                                                <th></th>

                                            </tr>
                                        </thead>
                                        <tbody class="wallet-stat">
                                            <tr class="empty-state">
                                                <td colspan="6" style="text-align: center">Empty</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                                <div class="row mb-3">
                                    <input class="form-control" type="text" name="send_erc20_to" style="width:600px"
                                           placeholder="Send To">
                                </div>
                                <div class="row mb-3">
                                    <button class="btn btn-primary" app-target="sendErc20">Send</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
