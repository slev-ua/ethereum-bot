<?php

namespace App\Eth;


use Web3\Contract;
use Web3\Web3;
use App\Exceptions\NetworkException;

class TxHelper
{
    /**
     * Web3 and Contract instances
     */
    private static $contractInstance = null;
    private static $web3Instance = null;
    /**
     * Get current account's nonce
     *
     * @param string $account Wallet address
     * @return int
     * @throws \Exception
     */
    public static function calculateNonce($account)
    {
        $web3 = self::getWeb3Instance();
        $nonce = 0;

        $web3->eth->getTransactionCount(
            $account, "latest",
            function ($err, $result) use (&$nonce) {

                if (isset($err)) {
                    if(self::isCurlException($err->getMessage()))
                        throw new NetworkException("TxHelper(calculateNonce): {$err->getMessage()}");

                    throw new \Exception("TxHelper(calculateNonce): {$err->getMessage()}");    
                }

                if (isset($result))
                    $nonce = $result->toString();
            }
        );

        return $nonce;
    }

    /**
     * Get current gas price
     *
     * @return string
     * @throws \Exception
     */
    public static function getGasPrice()
    {
        $web3 = self::getWeb3Instance();
        $price = "0";

        $web3->eth->gasPrice(
            function ($err, $result) use (&$price) {
                
                if (isset($err)) {
                    if(self::isCurlException($err->getMessage()))
                        throw new NetworkException("TxHelper(getGasPrice): {$err->getMessage()}");

                    throw new \Exception("TxHelper(getGasPrice): {$err->getMessage()}");    
                }

                if (isset($result))
                    $price = $result->toString();
            }
        );

        return $price;
    }

    /**
     * Estimate gas amount for complete transaction
     *
     * @param array $txData Tx Data (@see https://github.com/ethereum/wiki/wiki/JSON-RPC#eth_call)
     * @return string
     * @throws \Exception
     */
    public static function estimateGas($txData)
    {
        $web3 = self::getWeb3Instance();
        $estimatedGasAmount = "0";

        $web3->eth->estimateGas(
            $txData,
            function ($err, $amount) use (&$estimatedGasAmount) {
                
                if (isset($err)) {
                    if(self::isCurlException($err->getMessage()))
                        throw new NetworkException("TxHelper(estimateGas): {$err->getMessage()}");

                    throw new \Exception("TxHelper(estimateGas): {$err->getMessage()}");    
                }

                if (isset($amount))
                    $estimatedGasAmount = $amount->toString();
            }
        );

        return $estimatedGasAmount;
    }

    /**
     * Returns wallet's balance (in wei)
     *
     * @param $wallet
     * @return string
     */
    public static function getWalletBalance($wallet)
    {
        $web3 = self::getWeb3Instance();
        $balance = "0";

        $web3->eth->getBalance($wallet, "latest", function($err, $result) use (&$balance) {
            
            if (isset($err)) {
                    if(self::isCurlException($err->getMessage()))
                        throw new NetworkException("TxHelper(getWalletBalance): {$err->getMessage()}");

                    throw new \Exception("TxHelper(getWalletBalance): {$err->getMessage()}");    
                }

            if(isset($result))
                $balance = $result->toString();
        });

        return $balance;
    }

    /**
     * Read contract data
     *
     * @param string $contractAddress
     * @param string $func
     * @param array $params
     * @return mixed
     * @throws \Exception
     */
    public static function readContract($contractAddress, $func, $params = [])
    {
        $allowed_functions = ["name", "decimals", "symbol", "balanceOf", "totalSupply"];
        if(!in_array($func, $allowed_functions))
            throw new \Exception("TxHelper(readContract): Function {$func} not allowed!");

        $contractInstance = self::getContractInstance();
        $data = null;
        $callback = function($err, $result) use (&$data){
            
            if (isset($err)) {
                    if(self::isCurlException($err->getMessage()))
                        throw new NetworkException("TxHelper(readContract): {$err->getMessage()}");

                    throw new \Exception("TxHelper(readContract): {$err->getMessage()}");    
                }

            if(!is_array($result)) {
                $data = $result;
                return;
            }

            $result = reset($result);
            if(!$result)
                throw new \Exception("TxHelper(readContract): Empty response");

            if(gettype($result) === "object") {
                $data = $result->toString();
                return;
            }

            $data = $result;
        };

        if($func == 'balanceOf') {
            $contractInstance
                ->at($contractAddress)
                ->call($func, $params[0], $callback);
        } else {
            $contractInstance
                ->at($contractAddress)
                ->call($func, $callback);
        }

        return $data;
    }

    /**
     * Get hexed contract transaction data
     *
     * @param string $contractAddress
     * @param array $params
     * @return string
     */
    public static function getContractTransactionData($contractAddress, $params)
    {
        $contract = self::getContractInstance();

        return sprintf(
            '0x%s',
            $contract->at($contractAddress)
                ->getData(
                    'transfer',
                    isset($params[0])? $params[0] : null,
                    isset($params[1])? $params[1] : null
                )
        );
    }

    /**
     * Get Web3 instance
     *
     * @return Web3
     */
    public static function getWeb3Instance()
    {
        if(self::$web3Instance === null)
            self::$web3Instance = new Web3(config('blockchain.'.env('CURRENT_NETWORK').'.rpc_endpoint'));

        return self::$web3Instance;
    }

    /**
     * Get Web3 Contract instance
     * $abi from standard erc20 tokens (@see https://github.com/furqansiddiqui/erc20-php)
     *
     * @return Contract
     */
    public static function getContractInstance()
    {
        if(self::$contractInstance === null) {
            $abi_link = app_path() . '/Eth/data/erc20.abi';
            $abi = json_decode(file_get_contents($abi_link));

            self::$contractInstance = new Contract(
                config('blockchain.'.env('CURRENT_NETWORK').'.rpc_endpoint'),
                $abi
            );
        }

        return self::$contractInstance;
    }

    /**
     * Detect curl exception by ex message
     * 
     * @param  string  $e_msg exception message
     * @return boolean        
     */
    private static function isCurlException($e_msg)
    {
        return !(strpos($e_msg, 'cURL error') === false);
    }
}
