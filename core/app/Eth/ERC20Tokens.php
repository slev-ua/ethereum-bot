<?php

namespace App\Eth;


use Illuminate\Support\Facades\Log;
use Web3\Web3;
use Web3p\EthereumTx\Transaction;

class ERC20Tokens
{
    // gas for operation
    private $estimatedGas;
    private $gasPrice;

    // account's nonce
    private $nonce = null;

    /**
     * @var Web3 Eth Json RPC Api Layer
     */
    private $web3;

    /**
     * EthTokens constructor.
     */
    public function __construct()
    {
        $this->web3 = TxHelper::getWeb3Instance();
    }

    /**
     * Send multiple transactions
     *
     * @param string $from
     * @param string $to
     * @param string $private_key
     * @param array $tokens token data array (required contractAddress and tokenBalance)
     * @return array
     * @throws \Exception
     *
     * // TODO: move this method to 'Send Service'
     */
    public function sendMultipleTransactions($from, $to, $private_key, $tokens)
    {
        if($this->nonce === null)
            $this->nonce = TxHelper::calculateNonce($from);

        foreach($tokens as &$token) {

            // if not market to send
            if(!isset($token["send"]) || $token["send"] !== "true")
                continue;

            // if already sent
            if(isset($token['status']) && $token['status'] === 'success')
                continue;

            if($token['requiredGasPrice'] < 0)
                continue;

            $result = $this->sendTransaction(
                $from,
                $to,
                $private_key,
                $token["contractAddress"],
                $token["tokenBalance"]
            );

            // save result
            if($result["status"] == 'success') {
                $token["status"] = 'success';
                $token["txHash"] = $result['txHash'];
            } else {
                $token['status'] = 'failed';
                $token['description'] = $result['description'];
            }

            // increment nonce for next tx
            $this->nonce++;
        }

        return $tokens;
    }

    /**
     * Send amount {erc20} to some wallet
     *
     * @param string $from wallet from
     * @param string $to wallet to
     * @param string $private_key {$from} wallet's private key
     * @param $contractAddress
     * @param $tokensAmount
     * @return array
     */
    public function sendTransaction($from, $to, $private_key, $contractAddress, $tokensAmount)
    {
        try {

            $contractTxData = TxHelper::getContractTransactionData(
                $contractAddress,
                [$to, $tokensAmount]
            );

            // estimate gas and get it's price
            $this->estimatedGas = TxHelper::estimateGas([
                'from' => $from,
                'to' => $contractAddress,
                'value' => 0,
                'data' => $contractTxData
            ]);
            $this->gasPrice = TxHelper::getGasPrice();

            // get nonce for next tx
            if($this->nonce === null)
                $this->nonce = TxHelper::calculateNonce($from);

            // create and sign transaction
            $transaction = $this->createTransaction($from, $to, '0', $contractAddress, $contractTxData);
            $transaction = $transaction->sign($private_key);
        } catch (\Exception $e) {
            Log::debug($e->getMessage());

            return [
                "status" => "failed",
                "description" => $e->getMessage()
            ];
        }

        return [
            'status' => 'success',
            'txHash' => 'test'
        ];

        $result = [];

        // executing
        $this->web3->eth->sendRawTransaction(
            sprintf('0x%s', $transaction),
            function($err, $txHash) use (&$result) {
                if(isset($err)) {
                    Log::debug("EthTokens(sendRawTransaction): {$err->getMessage()}");
                    $result = [
                        'status' => 'failed',
                        'description' => $err->getMessage()
                    ];
                    return;
                }

                if(isset($txHash)) {
                    Log::debug("EthTokens(sendRawTransaction): transaction was placed. Hash - {$txHash}");

                    $result = [
                        'status' => 'success',
                        'txHash' => $txHash
                    ];
                }
            }
        );

        return $result;
    }

    /**
     * Create transaction Object
     *
     * @param string $from wallet from
     * @param string $to wallet to
     * @param int|float $amount amount (in ether)
     * @param string $contractAddress tokens holder contract address
     * @param string $data hexed contract transaction data
     * @return Transaction
     */
    private function createTransaction($from, $to, $amount, $contractAddress, $data)
    {
        return new Transaction([
            'from'      => $from,
            'to'        => $contractAddress,
            'value'     => Utils::toHex(Utils::toWei($amount, 'ether')->toString(), true),
            'nonce'     => Utils::toHex($this->nonce, true),
            'gasPrice'  => Utils::toHex($this->gasPrice, true),
            'gasLimit'  => Utils::toHex($this->estimatedGas, true),
            'chainId'   => config('blockchain.'.env('CURRENT_NETWORK').'.chain_id'),
            'data'      => $data
        ]);
    }
}
