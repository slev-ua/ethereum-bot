<?php

namespace App\Eth;


use Brick\Math\BigDecimal;
use Illuminate\Support\Facades\Log;
use Web3\Web3;
use Web3p\EthereumTx\Transaction;

// TODO: validate input data
class EthTokens
{
    // gas for operation
    private $estimatedGas;
    private $gasPrice;

    // account's nonce
    private $nonce;

    /**
     * @var Web3 Eth Json RPC Api Layer
     */
    private $web3;

    private $available_to_send = '';

    /**
     * EthTokens constructor.
     */
    public function __construct()
    {
        $this->web3 = TxHelper::getWeb3Instance();
    }

    /**
     * Send all available eth to address
     *
     * @param string $from
     * @param string $to
     * @param string $private_key
     * @return array|mixed
     */
    public function sendAllAvailableEth($from, $to, $private_key)
    {
        try {
            // estimate gas
            $this->estimatedGas = TxHelper::estimateGas(['from' => $from, 'to' => $to]);
            // set gas price to 4Gwei
            $this->gasPrice = "4000000000";
            // cur balance
            $acc_balance = TxHelper::getWalletBalance($from);
            if($acc_balance == 0)
                throw new \Exception("Wallet balance empty");

            // calculate max available founds to send
            $txCost = $this->estimatedGas * $this->gasPrice;
            $available_to_send = $acc_balance - $txCost;
            $this->available_to_send = strval($available_to_send);

            // get nonce for next tx
            $this->nonce = TxHelper::calculateNonce($from);


            // create and sign transaction
            $transaction = $this->createTransaction($from, $to, $available_to_send);
            $transaction = $transaction->sign($private_key);
        } catch (\Exception $e) {
            Log::debug($e->getMessage());
            Log::debug($e->getTraceAsString());

            return [
                "status" => "failed",
                "description" => $e->getMessage()
            ];
        }

        // executing
        $result = $this->sendRawTransaction($transaction);

        return $result;
    }

    /**
     * Send amount {eth} to some wallet
     *
     * @param string $from wallet from
     * @param string $to wallet to
     * @param int|float $amount amount (in ether)
     * @param string $private_key {$from} wallet's private key
     * @return array
     */
    public function sendTransaction($from, $to, $amount, $private_key)
    {
        try {
            // estimate gas
            $this->estimatedGas = TxHelper::estimateGas(['from' => $from, 'to' => $to]);

            // set gas price to 4Gwei
//            $this->gasPrice = TxHelper::getGasPrice();
            $this->gasPrice = "4000000000";
            // get nonce for next tx
            $this->nonce = TxHelper::calculateNonce($from);

            // create and sign transaction
            $transaction = $this->createTransaction($from, $to, $amount);
            $transaction = $transaction->sign($private_key);
        } catch (\Exception $e) {
            Log::debug($e->getMessage());

            return [
                "status" => "failed",
                "description" => $e->getMessage()
            ];
        }

        // executing
        $result = $this->sendRawTransaction($transaction);

        return $result;
    }

    /**
     * Create transaction Object
     *
     * @param string $from wallet from
     * @param string $to wallet to
     * @param int|float $amount amount (in ether)
     * @return Transaction
     */
    public function createTransaction($from, $to, $amount)
    {
        return new Transaction([
            'from' => $from,
            'to' => $to,
            'value' => ($amount > 1000000)? // if amount greater 1mil eth, it's probably wei
                Utils::toHex($this->available_to_send, true) :
                Utils::toHex(Utils::etherToWei($amount), true),
            'nonce' => Utils::toHex($this->nonce, true),
            'gasPrice' => Utils::toHex($this->gasPrice, true),
            'gasLimit' => Utils::toHex($this->estimatedGas, true),
            'chainId' => config('blockchain.'.env('CURRENT_NETWORK').'.chain_id')
        ]);

    }

    /**
     * Send raw transaction
     *
     * @param string $tx
     * @return mixed
     */
    private function sendRawTransaction(string $tx)
    {
        // executing
        $this->web3->eth->sendRawTransaction(
            sprintf('0x%s', $tx),
            function($err, $txHash) use (&$result) {
                if(isset($err)) {
                    Log::debug("EthTokens(sendRawTransaction): {$err->getMessage()}");
                    $result = [
                        'status' => 'failed',
                        'description' => $err->getMessage()
                    ];
                    return;
                }

                if(isset($txHash)) {
                    Log::debug("EthTokens(sendRawTransaction): transaction was placed. Hash - {$txHash}");

                    $result = [
                        'status' => 'success',
                        'txHash' => $txHash
                    ];
                }
            }
        );

        return $result;
    }
}