#

import sys, json

from eth_utils import encode_hex
from eth_keyfile import (
    load_keyfile,
    decode_keyfile_json
)

try:
#   get keyfile and password
    data = json.loads(sys.argv[1]);
    json_keyfile = json.loads(data['keystore'])
    password = data['pwd']

except Exception as ex:
    print('LOAD DATA ERROR')
    quit()


try:
#   try to decode (see https://github.com/ethereum/eth-keyfile)
    private_key = decode_keyfile_json(json_keyfile, password)
    private_key = encode_hex(private_key)

    print(private_key)
    quit()

except Exception as ex:
    print('DECODE DATA ERROR')
    quit()
