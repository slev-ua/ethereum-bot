<?php

namespace App\Eth;

use GuzzleHttp\Client;
use GuzzleHttp\ClientInterface;
use phpseclib\Math\BigInteger;

class TokensParser
{
    /** @var Client Http Client (Guzzle by default) */
    private $httpClient;

    /** API Endpoints */
    private static $api_root;
    const ERC20_LIST_ENDPOINT = '?module=account&action=tokentx&address={wallet}&startblock=0&endblock=999999999&sort=asc';

    private static $gasPrice = null;

    /**
     * TokensParser constructor.
     * @param ClientInterface|null $client
     */
    public function __construct(ClientInterface $client = null)
    {
        $this->httpClient = (isset($client))? $client : new Client();
        self::$api_root = config('blockchain.'.env('CURRENT_NETWORK').'.api_root');
    }

    /**
     * @param $wallet
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getTokenTransactionList($wallet)
    {
        $data = $this->request($this->makeEndpointLink($wallet));

        if($data['status'] !== '1')
            return [
                'status' => 'failed',
                'description' => $data['message']
            ];

        if(empty($data['result']))
            return [
                'status' => 'empty',
                'reason' => 'etherscan returned empty tx list'
            ];

        return ['status' => 'ok', 'tx' => $data['result']];
    }

    /**
     * Returns unique contracts addresses
     * from whole transactions list
     *
     * @param $wallet
     * @return array
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getContractsList($wallet)
    {
        $txData = $this->getTokenTransactionList($wallet);
        if($txData['status'] == 'failed' || $txData['status'] == 'empty')
            return [];

        $contractsList = [];

        foreach($txData['tx'] as $tx) {
            if(array_search($tx["contractAddress"], $contractsList) === false)
                $contractsList[] = $tx["contractAddress"];
        }

        return $contractsList;
    }

    /**
     * Return wallet's ERC20 tokens balance list
     *
     * @param $wallet
     * @param $contractAddress
     * @return mixed
     */
    public function getErc20TokenBalance($wallet, $contractAddress)
    {
        $tmp = $this->parseTokenData($contractAddress, $wallet);
        if($tmp)
            $tokData[] = $tmp;

        if(empty($tokData))
            return [
                'status' => 'empty',
                'reason' => 'Contract query return empty tokData'
            ];

        $tokensData['result'] = $tokData;

        $tokensData['status'] = 'ok';

        return $tokensData;
    }

    /**
     * Make each token data list
     *
     * @param $contractAddress
     * @param string $wallet owner wallet
     * @return mixed
     */
    private function parseTokenData($contractAddress, $wallet)
    {
        try {

            $balance = TxHelper::readContract($contractAddress, "balanceOf", [$wallet]);

            
            // Don't show, if all tokens was spend
            if($balance == 0)
                return false;

            $name = TxHelper::readContract($contractAddress, "name");
            $symbol = TxHelper::readContract($contractAddress, "symbol");
            $decimals = TxHelper::readContract($contractAddress, "decimals");

            try {
                $requiredGas = TxHelper::estimateGas([
                    'from'  => $wallet,
                    'to'    => $contractAddress,
                    'value' => 0,
                    'data'  => TxHelper::getContractTransactionData($contractAddress, [$wallet, $balance])
                ]);

                if(self::$gasPrice === null)
                    self::$gasPrice = TxHelper::getGasPrice();

                $reqGasPrice = Utils::weiToEther(
                    \Brick\Math\BigInteger::of(self::$gasPrice)->multipliedBy($requiredGas)->toInt()
                );
            }  catch(\Exception $e) {
                $requiredGas = -1;
                $reqGasPrice = -1;
                
            }

        } catch (\Exception $e) {
            return false;
        }

        return [
            'tokenName'     => $name,
            'tokenSymbol'   => $symbol,
            'tokenDecimals' => $decimals,
            'tokenBalance'  => $balance,

            'convertedTokenBalance' => Utils::numberConverter($balance, "div", $decimals),

            'requiredGas'   => $requiredGas,
            'requiredGasPrice'  => $reqGasPrice,

            'contractAddress' => $contractAddress,
        ];
    }

    /**
     * Make request
     *
     * @param string $link uri
     * @param string $method request method (GET by default)
     * @return mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    private function request($link, $method = "GET")
    {
        $response = $this->httpClient->request(
            $method,
            $link,
            ['verify' => false]
        );

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * Make api endpoint link
     *
     * @param string $wallet wallet address
     * @return string
     */
    private function makeEndpointLink($wallet)
    {
        return self::$api_root
            .str_replace('{wallet}', $wallet, self::ERC20_LIST_ENDPOINT);
    }
}
