<?php

namespace App\Eth;


use Brick\Math\BigDecimal;
use Brick\Math\RoundingMode;
use Web3p\EthereumUtil\Util;

class Utils extends \Web3\Utils
{

    /**
     * Decode keystore file, return  wallet's private key
     *
     * @param string $keystore_json
     * @param string $password
     * @return string
     * @throws \Exception
     */
    public static function decodeKeystoreFile($keystore_json, $password)
    {
        $script = app_path("Eth/priv_from_keystore.py");

        $data = ['keystore' => $keystore_json, 'pwd' => $password];
        $data = escapeshellarg(json_encode($data));
        dump($data);

        $result = shell_exec("python3 {$script} ".$data." 2>&1");
        dump($result);

        switch($result) {

            case "LOAD DATA ERROR\n":
                throw new \Exception("Load data error");

            case "DECODE DATA ERROR\n":
                throw new \Exception("Decode data error");

            default:
                if(empty($result))
                    throw new \Exception("Empty result returned");

                // truncate last '\n'?
                return str_replace("\n", "", substr($result, 2));
        }
    }

    /**
     * Get wallet address from private key
     *
     * @param $priv
     * @return bool|string
     */
    public static function addressFromPrivateKey($priv)
    {
        $utils = new Util();

        try {
            $pub_key = $utils->privateKeyToPublicKey($priv);
            return $utils->publicKeyToAddress($pub_key);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Convert wei to ether
     *
     * @param int $amount wei amount
     * @param int $scale
     * @return float
     */
    public static function weiToEther($amount, $scale = 5)
    {
        return self::numberConverter($amount, 'div', $scale);
    }

    /**
     * Convert ether to wei
     *
     * @param float|int $amount ether amount
     * @return int
     */
    public static function etherToWei($amount)
    {
        return self::numberConverter($amount, 'mul');
    }

    /**
     * Multiple and divide big numbers by pow(10, $decimals)
     *
     * <code>
     * <?php
     *      echo numberConverter(1.025, 'mul'); // outputs 1025000000000000000 (like 1.025 ether to wei)
     *      echo numberConverter(1.025, 'mul', 10); // outputs 10250000000 (like 1 coin with 10decimals to it's base number)
     *
     *      echo numberConverter(1025000000000000, 'div'); // outputs 1.025 (like $x wei to ether)
     *
     * @param int|float $num
     * @param string $operation
     * @param int $scale
     * @param int $decimals
     * @return float|int
     */
    public static function numberConverter($num, $operation, $scale = 5, $decimals = 18)
    {
        if($operation == "div")
            return BigDecimal::of($num)
                ->dividedBy(pow(10, $decimals), $scale, RoundingMode::DOWN)
                ->toFloat();

        return BigDecimal::of($num)
            ->multipliedBy(pow(10, $decimals))
            ->toInt();
    }
}
