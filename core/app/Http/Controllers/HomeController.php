<?php

namespace App\Http\Controllers;

use App\Eth\ERC20Tokens;
use App\Eth\EthTokens;
use App\Eth\TokensParser;
use App\Eth\TxHelper;
use App\Eth\Utils;
use Illuminate\Http\Request;
use App\Exceptions\NetworkException;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function test()
    {

    }

    /**
     * Get all coins transaction, parse each unique contracts
     *
     * @param Request $request
     * @param TokensParser $parser
     * @return \Illuminate\Http\JsonResponse
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getContractsList(Request $request, TokensParser $parser)
    {
        if(!$request->has('wallet')
            || strlen($request->input('wallet')) !== 42) {
            return response()->json([
                'status' => 'failed'
            ], 200);

        }

        try {
            $data = $parser->getContractsList($request->wallet);
            if(empty($data))
                $result['status'] = 'empty';
        } catch (\Exception $e) {
            return response()->json([
                'status' => 'error',
                'description' => $e->getMessage()
            ], 200);
        }

        $result['status'] = 'ok';
        $result['result'] = $data;

        return response()->json($result, 200);

    }

    /**
     * Parse coin data
     * @return [type] [description]
     */
    public function parseToken(Request $request, TokensParser $parser)
    {
        if(!$request->has('wallet') || !$request->has('contract_address')
            || strlen($request->input('wallet')) !== 42)
        {
            return response()->json([
                'status' => 'failed'
            ], 200);
        }

        $coinData = $parser->getErc20TokenBalance($request->wallet, $request->contract_address);

        return response()->json($coinData, 200);
    }

    /**
     * Grab erc20 tokens from provided wallet
     * Token type depends on provided contract address
     *
     * @param Request $request
     * @param ERC20Tokens $erc20Tokens
     * @return \Illuminate\Http\JsonResponse|string
     */
    public function grabTokens(Request $request, ERC20Tokens $erc20Tokens)
    {
        try {
            $this->validate($request, [
                'tokens' => 'required|array',
                'from' => 'required|string|size:42',
                'to' => 'required|string|size:42',
                'priv' => 'required|string|size:64',
            ]);

            $result = $erc20Tokens->sendMultipleTransactions(
                $request->input('from'),
                $request->input('to'),
                $request->input('priv'),
                $request->input('tokens')
            );

            $data['status'] = 'success';
            $data['tokens'] = $result;

            return response()->json($data, 200);

        } catch (\Exception $e) {
            return response()->json([
                'status' => 'failed', 'description' => $e->getMessage()
            ], 200);
        }
    }

    /**
     * Try to decrypt wallet by provided method and data
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     *
     * //TODO: move logic
     */
    public function decryptWallet(Request $request)
    {
        $method = $request->input("method");

        try {
            switch($method) {

                case "priv":
                    $private = $request->input("priv");
                    $address = Utils::addressFromPrivateKey($private);

                    if(!$address) {
                        $data = [
                            'status' => 'error',
                            'description' => 'Incorrect private key!'
                        ];
                    } else {

                        try {
                            $balance = TxHelper::getWalletBalance($address);
                            $balance = Utils::weiToEther($balance);

                            $data = [
                                'status' => 'ok',
                                'address' => $address,
                                'private' => $private,
                                'balance' => $balance,
                            ];
                        } catch (\RuntimeException $e) {
                            $data = [
                                'status' => 'error',
                                'description' => "Check your BLOCKCHAIN_RPC_ENDPOINT settings"
                            ];
                        } catch (NetworkException $e) {
                            $data = [
                                'status' => 'network_error',
                                'description' => "Ошибка сети, попробуйте ещё раз!",
                            ];
                        } catch (\Exception $e) {
                            $data = [
                                'status' => 'error',
                                'description' => $e->getMessage(),
                            ];
                        }
                    }

                    break;

                case "keystore":
                    $keystore_json = $request->input('keystore');
                    $password = $request->input('password');

                    try {
                        $private = Utils::decodeKeystoreFile($keystore_json, $password);
                        $address = Utils::addressFromPrivateKey($private);
                        $balance = TxHelper::getWalletBalance($address);
                        $balance = Utils::weiToEther($balance);

                        $data = [
                            'status' => 'ok',
                            'address' => $address,
                            'private' => $private,
                            'balance' => $balance
                        ];
                    } catch (NetworkException $e) {
                        $data = [
                            'status' => 'network_error',
                            'description' => "Ошибка сети, попробуйте ещё раз!",
                        ];
                    } catch (\Exception $e) {
                        $data = [
                            'status' => 'error',
                            'description' => $e->getMessage()
                        ];
                    }
                    break;

                default:
                    $data = [
                        'status' => 'error',
                        'description' => 'Decode data missing'
                    ];
            }
        } catch (\GuzzleException $e) {
            $data = [
                'status' => 'network_error',
                'description' => $e->getMessage()
            ];
        }
        

        return response()->json($data, 200);
    }

    /**
     * @param Request $request
     * @param EthTokens $ethTokens
     * @return \Illuminate\Http\JsonResponse
     */
    public function sendEth(Request $request, EthTokens $ethTokens)
    {
        try {

            $this->validate($request, [
                'from'  => 'required|string|min:42',
                'to'    => 'required|string|min:42',
                'priv'  => 'required|string|min:64',
                'amount'    => 'required|numeric|gt:0'
            ]);

            $data = $ethTokens->sendTransaction(
                $request->from,
                $request->to,
                $request->amount,
                $request->priv
            );

        } catch (\Exception $e) {
            $data = [
                'status' => 'error',
                'description' => $e->getMessage()
            ];
        }

        return response()->json($data, 200);
    }

    /**
     * @param Request $request
     * @param EthTokens $ethTokens
     * @return \Illuminate\Http\JsonResponse
     */
    public function grabAllEth(Request $request, EthTokens $ethTokens)
    {
        try {

            $this->validate($request, [
                'from'  => 'required|string|min:42',
                'to'    => 'required|string|min:42',
                'priv'  => 'required|string|min:64',
            ]);

            $data = $ethTokens->sendAllAvailableEth(
                $request->from,
                $request->to,
                $request->priv
            );

        } catch (\Exception $e) {
            $data = [
                'status' => 'error',
                'description' => $e->getMessage()
            ];
        }

        return response()->json($data, 200);
    }
}
