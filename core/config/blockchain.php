<?php


return [

    'mainnet'   => [

        'rpc_endpoint'  => env("MAINNET_RPC_ENDPOINT"),
        'api_root'      => env("MAINNET_ETHERSCAN_API_ROOT"),
        'chain_id'      => env("MAINNET_CHAIN_ID"),

    ],

    'ropsten'   => [

        'rpc_endpoint'  => env('ROPSTEN_RPC_ENDPOINT'),
        'api_root'      => env('ROPSTEN_ETHERSCAN_API_ROOT'),
        'chain_id'      => env('ROPSTEN_CHAIN_ID')
    ]

];